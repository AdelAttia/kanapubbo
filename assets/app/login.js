window.hostServer = "http://vps130625.ovh.net/api/";

$(document).ready(function () {

    $("#login-button").click(function () {
        $("#login-form").submit();
    });


    $(document).on('submit', '#login-form', function (event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: hostServer + "api-token-auth/",
            data: {username: $("#username").val(), password: $("#password").val()}
        })
            .success(function (data) {
                saveData('kanapubToken', data.token);
                $.ajax({
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "JWT " + data.token);
                    },
                    url: hostServer + "whoami"
                })
                    .success(function (data) {
                        if(data.is_superuser) {
                            data.role = 1;
                        } else if (data.publisher_users.length > 0) {
                            data.role = 2;
                        } else {
                            data.role = 3;
                        }
                        saveData("userInfo", JSON.stringify(data));
                        window.location = "index.html";
                    });
            });
    })

});


aa("test");


function saveData(key, data) {
    if (window.localStorage) {
        window.localStorage.setItem(key, data);
    } else {
        $.cookie(key, data);
    }
}

function getData(key) {
    if (window.localStorage) {
        data = window.localStorage.getItem(key);
    } else {
        data = $.cookie(key);
    }
    return data;
}


function aa(a) {
    console.log(a);
};