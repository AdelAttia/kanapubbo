window.Dimension = Backbone.Model.extend({
    urlRoot: window.hostServer + 'dimension/'
});

window.DimensionCollection = Backbone.Collection.extend({
    model: Dimension,
    url: window.hostServer + "dimension/"
});