window.AdvertiserUser = Backbone.Model.extend({
    urlRoot: window.hostServer + 'advertiser_users/'
});

window.AdvertiserUserCollection = Backbone.Collection.extend({
    model: AdvertiserUser,
    url: window.hostServer + "advertiser_users/"
});