window.PublisherType = Backbone.Model.extend({
    urlRoot: window.hostServer + 'publisher_type/'
});

window.PublisherTypeCollection = Backbone.Collection.extend({
    model: PublisherType,
    url: window.hostServer + "publisher_type/"
});