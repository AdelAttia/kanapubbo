window.PublisherWebsite = Backbone.Model.extend({
    urlRoot: window.hostServer + 'publisher_website/'
});

window.PublisherWebsiteCollection = Backbone.Collection.extend({
    model: PublisherWebsite,
    url: window.hostServer + "publisher_website/"
});