window.Advertiser = Backbone.Model.extend({
    urlRoot: window.hostServer + 'advertiser/'
});

window.AdvertiserCollection = Backbone.Collection.extend({
    model: Advertiser,
    url: window.hostServer + "advertiser/"
});