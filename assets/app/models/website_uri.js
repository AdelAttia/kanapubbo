window.Uri = Backbone.Model.extend({
    urlRoot: window.hostServer + 'website_uri/'
});

window.UriCollection = Backbone.Collection.extend({
    model: Uri,
    url: window.hostServer + "website_uri/"
});