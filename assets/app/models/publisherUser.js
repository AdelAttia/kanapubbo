window.PublisherUser = Backbone.Model.extend({
    urlRoot: window.hostServer + 'publisher_users/'
});

window.PublisherUserCollection = Backbone.Collection.extend({
    model: PublisherUser,
    url: window.hostServer + "publisher_users/"
});