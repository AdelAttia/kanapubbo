window.Zone = Backbone.Model.extend({
    urlRoot: window.hostServer + 'zone/'
});

window.ZoneCollection = Backbone.Collection.extend({
    model: Zone,
    url: window.hostServer + "zone/"
});