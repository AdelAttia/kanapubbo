window.Campaign = Backbone.Model.extend({
    urlRoot: window.hostServer + 'campaign/'
});

window.CampaignCollection = Backbone.Collection.extend({
    model: Campaign,
    url: window.hostServer + "campaign/"
});