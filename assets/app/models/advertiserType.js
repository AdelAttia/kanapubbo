window.AdvertiserType = Backbone.Model.extend({
    urlRoot: window.hostServer + 'advertiser_type/'
});

window.AdvertiserTypeCollection = Backbone.Collection.extend({
    model: AdvertiserType,
    url: window.hostServer + "advertiser_type/"
});