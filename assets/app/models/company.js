window.Company = Backbone.Model.extend({
    urlRoot: window.hostServer + 'company/'
});

window.CompanyCollection = Backbone.Collection.extend({
    model: Company,
    url: window.hostServer + "company/"
});