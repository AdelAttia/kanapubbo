window.Banner = Backbone.Model.extend({
    urlRoot: window.hostServer + 'banner/'
});

window.BannerCollection = Backbone.Collection.extend({
    model: Banner,
    url: window.hostServer + "banner/"
});