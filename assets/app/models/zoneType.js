window.ZoneType = Backbone.Model.extend({
    urlRoot: window.hostServer + 'zone_type/'
});

window.ZoneTypeCollection = Backbone.Collection.extend({
    model: ZoneType,
    url: window.hostServer + "zone_type/"
});