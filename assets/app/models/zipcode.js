window.Zipcode = Backbone.RelationalModel.extend({
    urlRoot: window.hostServer + 'zip_code/'
});

window.ZipcodeCollection = Backbone.Collection.extend({
    model: Company,
    url: window.hostServer + "zip_code/"
});