window.BannerType = Backbone.Model.extend({
    urlRoot: window.hostServer + 'banner_type/'
});

window.BannerTypeCollection = Backbone.Collection.extend({
    model: BannerType,
    url: window.hostServer + "banner_type/"
});