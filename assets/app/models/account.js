window.Account = Backbone.Model.extend({
    urlRoot: window.hostServer + 'account/'
});

window.AccountCollection = Backbone.Collection.extend({
    model: Account,
    url: window.hostServer + "account/"
});