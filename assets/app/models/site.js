window.Site = Backbone.Model.extend({
    urlRoot: window.hostServer + 'site/'
});

window.SiteCollection = Backbone.Collection.extend({
    model: Site,
    url: window.hostServer + "site/"
});