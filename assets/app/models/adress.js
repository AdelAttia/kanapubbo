window.Adress = Backbone.Model.extend({
    urlRoot: window.hostServer + 'adress/'
});

window.AdressCollection = Backbone.Collection.extend({
    model: Adress,
    url: window.hostServer + "adress/"
});