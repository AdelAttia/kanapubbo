window.Publisher = Backbone.Model.extend({
    urlRoot: window.hostServer + "publisher/",
    relations: [{
        type: Backbone.HasOne,
        key: 'company',
        relatedModel: 'Company'
    }, {
        type: Backbone.HasMany,
        key: 'users',
        relatedModel: 'User'
    }, {
        type: Backbone.HasMany,
        key: 'sites',
        relatedModel: 'Site'
    }]
});

window.PublisherCollection = Backbone.Collection.extend({
    model: Publisher,
    urlRoot: window.hostServer + "publisher/",
    url: window.hostServer + "publisher/"
});