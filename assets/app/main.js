

var userType = 1; // admin
if (userInfo.role == 1) {
    $(".adminMenu").show();
} else if(userInfo.role == 2) {
    $(".publisherMenu").show();
} else {
    $(".advertiserMenu").show();
}

function aa(a) {
    console.log(a);
};



$(document).ready(function() {



});





// setting Router
window.Router = Backbone.Router.extend({

    routes: {
        "home"       : "home",
        "publishers" : "publishers",

        // account routes
        "accounts"          : "accounts",
        "accounts/edit/:id" : "accountsEdit",
        "accounts/new"      : "accountsEdit",

        // company routes
        "companies"          : "companies",
        "companies/edit/:id" : "companiesEdit",
        "companies/new"      : "companiesEdit",

        // company routes
        "users"          : "users",
        "users/edit/:id" : "usersEdit",
        "users/new" : "usersEdit",

        // advertiser routes
        "advertisers"          : "advertisers",
        "advertisers/edit/:id" : "advertisersEdit",
        "advertisers/new"      : "advertisersNew",

        // publisher routes
        "publishers"          : "publishers",
        "publishers/edit/:id" : "publishersEdit",
        "publishers/new"      : "publishersNew",

        // publisher's websites
        "sites"          : "sites",
        "sites/edit/:id" : "sitesEdit",
        "sites/new"      : "sitesEdit",

        // publisher's websites uris
        "uris"          : "uris",
        "uris/edit/:id" : "urisEdit",
        "uris/new"      : "urisEdit",

        // publisher's zones
        "zones"          : "zones",
        "zones/edit/:id" : "zonesEdit",
        "zones/new"      : "zonesEdit",

        // advertiser's campaigns
        "campaigns"          : "campaigns",
        "campaigns/edit/:id" : "campaignsEdit",
        "campaigns/new"      : "campaignsEdit",

        // advertiser's banners
        "banners"          : "banners",
        "banners/edit/:id" : "bannersEdit",
        "banners/new"      : "bannersEdit"
    },

    initialize: function () {
    },

    home: function () {
        this.homeView = new HomeView();
        $("#page-content").html(this.homeView.el);
        //this.headerView.select('home-menu');
    },

    publishers : function () {
        this.publisherListView = new PublisherListView();
        $("#page-content").html(this.publisherListView.el);
    },

    accounts : function () {
        this.accountListView = new AccountListView();
        $("#page-content").html(this.accountListView.el);
    },
    accountsEdit : function (id) {
        this.accountEditView = new AccountEditView();
        if(id) {
            this.accountEditView.render(id);
        } else {
            this.accountEditView.render(null);
        }
        $("#page-content").html(this.accountEditView.el);
    },

    companies : function () {
        this.companyListView = new CompanyListView();
        $("#page-content").html(this.companyListView.el);
    },
    companiesEdit : function (id) {
        this.companyEditView = new CompanyEditView();
        if(id) {
            this.companyEditView.render(id);
        } else {
            this.companyEditView.render(null);
        }
        $("#page-content").html(this.companyEditView.el);
    },

    users : function () {
        this.userListView = new UserListView();
        $("#page-content").html(this.userListView.el);
    },
    usersEdit : function (id) {
        this.userEditView = new UserEditView();
        if(id) {
            this.userEditView.render(id);
        } else {
            this.userEditView.render(null);
        }
        $("#page-content").html(this.userEditView.el);
    },

    advertisers : function () {
        this.advertiserListView = new AdvertiserListView();
        $("#page-content").html(this.advertiserListView.el);
    },
    advertisersEdit : function (id) {
        this.advertiserEditView = new AdvertiserEditView();
        this.advertiserEditView.render(id);
        $("#page-content").html(this.advertiserEditView.el);
    },
    advertisersNew : function () {
        this.advertiserNewView = new AdvertiserNewView();
        this.advertiserNewView.render();
        $("#page-content").html(this.advertiserNewView.el);
    },


    publishers : function () {
        this.publisherListView = new PublisherListView();
        $("#page-content").html(this.publisherListView.el);
    },
    publishersEdit : function (id) {
        this.publisherEditView = new PublisherEditView();
        this.publisherEditView.render(id);
        $("#page-content").html(this.publisherEditView.el);
    },
    publishersNew : function () {
        this.publisherNewView = new PublisherNewView();
        this.publisherNewView.render();
        $("#page-content").html(this.publisherNewView.el);
    },

    sites : function () {
        this.publisherWebsiteListView = new PublisherWebsiteListView();
        $("#page-content").html(this.publisherWebsiteListView.el);
    },
    sitesEdit : function (id) {
        this.publisherWebsiteEditView = new PublisherWebsiteEditView();
        if(id) {
            this.publisherWebsiteEditView.render(id);
        } else {
            this.publisherWebsiteEditView.render(null);
        }
        $("#page-content").html(this.publisherWebsiteEditView.el);
    },

    uris : function () {
        this.uriListView = new UriListView();
        $("#page-content").html(this.uriListView.el);
    },
    urisEdit : function (id) {
        this.uriEditView = new UriEditView();
        if(id) {
            this.uriEditView.render(id);
        } else {
            this.uriEditView.render(null);
        }
        $("#page-content").html(this.uriEditView.el);
    },

    zones : function () {
        this.zoneListView = new ZoneListView();
        $("#page-content").html(this.zoneListView.el);
    },
    zonesEdit : function (id) {
        this.zoneEditView = new ZoneEditView();
        if(id) {
            this.zoneEditView.render(id);
        } else {
            this.zoneEditView.render(null);
        }
        $("#page-content").html(this.zoneEditView.el);
    },

    campaigns : function () {
        this.campaignListView = new CampaignListView();
        $("#page-content").html(this.campaignListView.el);
    },
    campaignsEdit : function (id) {
        this.campaignEditView = new CampaignEditView();
        if(id) {
            this.campaignEditView.render(id);
        } else {
            this.campaignEditView.render(null);
        }
        $("#page-content").html(this.campaignEditView.el);
    },

    banners : function () {
        this.bannerListView = new BannerListView();
        $("#page-content").html(this.bannerListView.el);
    },
    bannersEdit : function (id) {
        this.bannerEditView = new BannerEditView();
        if(id) {
            this.bannerEditView.render(id);
        } else {
            this.bannerEditView.render(null);
        }
        $("#page-content").html(this.bannerEditView.el);
    }

});


templateLoader.load(
    [
        "HomeView",
        "AccountListView",
        "AccountEditView",
        "CompanyListView",
        "CompanyEditView",
        "UserListView",
        "UserEditView",
        "AdvertiserListView",
        "AdvertiserEditView",
        "AdvertiserNewView",
        "PublisherListView",
        "PublisherEditView",
        "PublisherNewView",
        "UserInfosView",
        "PublisherWebsiteListView",
        "PublisherWebsiteEditView",
        "UriListView",
        "UriEditView",
        "ZoneListView",
        "ZoneEditView",
        "CampaignListView",
        "CampaignEditView",
        "BannerListView",
        "BannerEditView",
    ],
    function () {
        app = new Router();
        // {pushState: true} ??
        Backbone.history.start();

        //user infos view
        this.userInfosView = new UserInfosView();
        $("#user-infos").html(this.userInfosView.el);

    });