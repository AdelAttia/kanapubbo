window.AdvertiserListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteAdvertiser'
    },

    render: function () {
        var advertisers = new AdvertiserCollection();
        var that = this;
        advertisers.fetch({
            success: function() {
                that.$el.html(that.template({
                    advertisers: advertisers.toJSON()
                }));
            }
        });
        return that;
    },

    deleteAdvertiser: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var advertiser = new Advertiser( {id : $(ev.currentTarget).data("id")} )
                advertiser.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});