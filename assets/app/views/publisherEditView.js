window.PublisherEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #publisherEditForm': 'savePublisher',
        'click #addUser': 'addUser',
        'submit #addUserForm': 'confirmAdd',
        'click .delete' : 'removeUser'
    },
    savePublisher: function (ev) {
        var publisher = new Publisher();

        var publisherDetails = {
            "id"      : $("#publisher-id").val(),
            "company" : {
                "id"  : $("#company").val()
            },
            "name"    : $("#name").val(),
            "type"    : $("#type").val(),
            "rcs"     : $("#rcs").val(),
            "serial"  : $("#serial").val()
        };

        publisher.save(publisherDetails, {
            success: function () {
                Backbone.history.navigate('#publishers', {trigger:true});
            }
        });
        return false;
    },
    removeUser: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Cet utilisateur n'appartiendra plus à l'éditeur, voulez-vous continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var userId = $(ev.currentTarget).data("id");
                var publishersList = [];

                var publisherUserCollection = new PublisherUserCollection().fetch({
                    success: function(publishers) {
                        publishers = publishers.toJSON();
                        _.each(publishers, function(a) {
                            publishersList.push(a);
                        })
                        var a = _.findWhere(publishersList, {user : userId});
                        var idToDelete = a.id;

                        var publisherUser = new PublisherUser({id: idToDelete});
                        publisherUser.destroy({
                            success: function () {
                                swal("Supprimé!", "L'utilisateur a été supprimé avec succès.", "success");
                                that.render($("#publisher-id").val());
                            }
                        });
                    }
                });

            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    },

    confirmAdd: function (ev) {
        var that = this;
        var publisherDetails = $(ev.currentTarget).serializeObject();
        var publisherUser    = new PublisherUser();
        publisherUser.save(publisherDetails, {
            success: function () {
                $('.users-list-modal').modal('toggle');
                //Backbone.history.navigate('#publishers', {trigger:true});
                that.render($("#publisher-id").val());
            },
            error: function() {
                $('.users-list-modal').modal('toggle');
                swal({   title: "Erreur d'ajout",   text: "Il se peut que l'utilisateur est déjà affecté à un autre éditeur",   timer: 2000 });
                that.render($("#publisher-id").val());
            }
        });
        return false;
    },

    addUser: function (ev) {
        var publisherDetails = $(ev.currentTarget).serializeObject();
        var allUsers = new User();
        publisher.save(publisherDetails, {
            success: function () {
                Backbone.history.navigate('#publishers', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var publisherTypes = new PublisherTypeCollection();
        var that = this;
        publisherTypes.fetch({
            success: function(publisherTypes) {

                var companies = new CompanyCollection();
                companies.fetch({

                    success: function (companies) {

                        var publisher = new Publisher({id : id});
                        publisher.fetch({

                            success: function(publisher) {

                                var users = new User();
                                users.fetch({

                                    success: function(users) {

                                        that.$el.html(that.template({
                                            publisherTypes: publisherTypes.toJSON(),
                                            companies      : companies.toJSON(),
                                            users          : users.toJSON(),
                                            publisher     : publisher.toJSON()
                                        }));

                                }});
                        }});
                }});
        }});

        return this;
    }
});