window.BannerEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #bannerEditForm': 'saveBanner'
    },
    saveBanner: function (ev) {
        var form = new FormData($('#bannerEditForm')[0]);
        if($("#id").val()) {
            $.ajax({
                url: hostServer + 'banner/' + $("#id").val(),
                type: 'PUT',
                success: function () {
                    Backbone.history.navigate('#banners', {trigger:true});
                },
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });
        } else {
            $.ajax({
                url: hostServer + 'banner/',
                type: 'POST',
                success: function () {
                    Backbone.history.navigate('#banners', {trigger:true});
                },
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });
        }
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var banner = new Banner({id: id});
            banner.fetch({
                success: function(banner) {

                    var dimension = new Dimension();
                    dimension.fetch({
                        success: function(dimensions) {

                            var bannerType = new BannerType();
                            bannerType.fetch({
                                success: function(bannersTypes) {

                                    that.$el.html(that.template({
                                        banner     : banner .toJSON(),
                                        dimensions : dimensions.toJSON(),
                                        types      : bannersTypes .toJSON()
                                    }));
                            }});
                    }});
            }});
        } else {


            var dimension = new Dimension();
            dimension.fetch({
                success: function(dimensions) {

                    var bannerType = new BannerType();
                    bannerType.fetch({
                        success: function(bannersTypes) {

                            that.$el.html(that.template({
                                banner     : null,
                                dimensions : dimensions.toJSON(),
                                types      : bannersTypes .toJSON()
                            }));
                        }});
                }});
        }

        return that;
    }
});