window.CompanyListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteCompany'
    },

    render: function () {
        var companies = new CompanyCollection();
        var that = this;
        companies.fetch({
            success: function() {
                console.log(companies.toJSON())
                that.$el.html(that.template({
                    companies: companies.toJSON()
                }));
            }
        });
        return that;
    },

    deleteCompany: function (ev) {
        var that = this;


        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var company = new Company( {id : $(ev.currentTarget).data("id")} )
                company.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render()
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });


        return false;
    }
});