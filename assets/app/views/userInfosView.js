window.UserInfosView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click #logout': 'logout'
    },

    render: function () {
        this.$el.html(this.template({
            user: window.userInfo
        }));
        return this;
    },

    logout: function (ev) {
        var that = this;


        $.ajax({
            type: "GET",
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "JWT " + getData("kanapubToken"));
            },
            url: hostServer + "logout"
        })
        .always(function (data) {
            localStorage.removeItem("kanapubToken");
            $.cookie("kanapubToken", null, {path: '/'});
            window.location.href = "login.html";
        });


        return false;
    }
});