window.AdvertiserEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #advertiserEditForm': 'saveAdvertiser',
        'click #addUser': 'addUser',
        'submit #addUserForm': 'confirmAdd',
        'click .delete' : 'removeUser'
    },
    saveAdvertiser: function (ev) {
        var advertiser = new Advertiser();

        var advertiserDetails = {
            "id" : $("#advertiser-id").val(),
            "company": {
                "id": $("#company").val()
            },
            "name": $("#name").val(),
            "type": $("#type").val()
        };

        advertiser.save(advertiserDetails, {
            success: function () {
                Backbone.history.navigate('#advertisers', {trigger:true});
            }
        });
        return false;
    },
    removeUser: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Cet utilisateur n'appartiendra plus à l'annonceur, voulez-vous continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var userId = $(ev.currentTarget).data("id");
                var advertisersList = [];

                var advertiserUserCollection = new AdvertiserUserCollection().fetch({
                    success: function(advertisers) {
                        advertisers = advertisers.toJSON();
                        _.each(advertisers, function(a) {
                            advertisersList.push(a);
                        })
                        var a = _.findWhere(advertisersList, {user : userId});
                        var idToDelete = a.id;

                        var advertiserUser = new AdvertiserUser({id: idToDelete});
                        advertiserUser.destroy({
                            success: function () {
                                swal("Supprimé!", "L'utilisateur a été supprimé avec succès.", "success");
                                that.render($("#advertiser-id").val());
                            }
                        });
                    }
                });

            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    },

    confirmAdd: function (ev) {
        var that = this;
        var advertiserDetails = $(ev.currentTarget).serializeObject();
        var advertiserUser    = new AdvertiserUser();
        advertiserUser.save(advertiserDetails, {
            success: function () {
                $('.users-list-modal').modal('toggle');
                //Backbone.history.navigate('#advertisers', {trigger:true});
                that.render($("#advertiser-id").val());
            },
            error: function() {
                $('.users-list-modal').modal('toggle');
                swal({   title: "Erreur d'ajout",   text: "Il se peut que l'utilisateur est déjà affecté à un autre annonceur",   timer: 2000 });
                that.render($("#advertiser-id").val());
            }
        });
        return false;
    },

    addUser: function (ev) {
        var advertiserDetails = $(ev.currentTarget).serializeObject();
        var allUsers = new User();
        advertiser.save(advertiserDetails, {
            success: function () {
                Backbone.history.navigate('#advertisers', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var advertiserTypes = new AdvertiserTypeCollection();
        var that = this;
        advertiserTypes.fetch({
            success: function(advertiserTypes) {

                var companies = new CompanyCollection();
                companies.fetch({

                    success: function (companies) {

                        var advertiser = new Advertiser({id : id});
                        advertiser.fetch({

                            success: function(advertiser) {

                                var users = new User();
                                users.fetch({

                                    success: function(users) {

                                        that.$el.html(that.template({
                                            advertiserTypes: advertiserTypes.toJSON(),
                                            companies      : companies.toJSON(),
                                            users          : users.toJSON(),
                                            advertiser     : advertiser.toJSON()
                                        }));

                                }});
                        }});
                }});
        }});

        return this;
    }
});