window.AdvertiserNewView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'submit #advertiserEditForm': 'saveAdvertiser'
    },
    saveAdvertiser: function (ev) {
        var advertiserDetails = {
            "company": {
                "id": $("#company").val()
            },
            "name": $("#name").val(),
            "type": $("#type").val()
        };
        var advertiser = new Advertiser();;

        advertiser.save(advertiserDetails, {
            success: function () {
                Backbone.history.navigate('#advertisers', {trigger:true});
            }
        });
        return false;
    },

    render: function () {
        var advertiserTypes = new AdvertiserTypeCollection();
        var that = this;
        advertiserTypes.fetch({
            success: function() {

                var companies = new CompanyCollection();
                companies.fetch({

                    success: function () {
                        that.$el.html(that.template({
                            advertiserTypes: advertiserTypes.toJSON(),
                            companies: companies.toJSON()
                        }));
                    }
                })

            }});

        return this;
    }
});