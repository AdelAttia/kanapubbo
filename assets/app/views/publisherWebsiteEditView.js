window.PublisherWebsiteEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #websiteEditForm': 'savePublisherWebsite'
    },
    savePublisherWebsite: function (ev) {
        var publisherWebsiteDetails = $(ev.currentTarget).serializeObject();
        var publisherWebsite = new PublisherWebsite();
        publisherWebsite.save(publisherWebsiteDetails, {
            success: function () {
                Backbone.history.navigate('#sites', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var publisherWebsite = new PublisherWebsite({id: id});
            //var publisherWebsite = PublisherWebsite.find({id : id});
            publisherWebsite.fetch({
                success: function(publisherWebsite) {
                    that.$el.html(that.template({
                        website: publisherWebsite.toJSON()
                    }));
                }
            });
        } else {
            that.$el.html(that.template({ website: null }));
        }

        return that;
    }
});