window.PublisherNewView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'submit #publisherEditForm': 'savePublisher'
    },
    savePublisher: function (ev) {
        var publisherDetails = {
            "company": {
                "id": $("#company").val()
            },
            "name": $("#name").val(),
            "type": $("#type").val(),
            "rcs": $("#rcs").val(),
            "serial": $("#serial").val()
        };
        var publisher = new Publisher();

        publisher.save(publisherDetails, {
            success: function () {
                Backbone.history.navigate('#publishers', {trigger:true});
            }
        });
        return false;
    },

    render: function () {
        var publisherTypes = new PublisherTypeCollection();
        var that = this;
        publisherTypes.fetch({
            success: function() {

                var companies = new CompanyCollection();
                companies.fetch({

                    success: function () {
                        that.$el.html(that.template({
                            publisherTypes: publisherTypes.toJSON(),
                            companies: companies.toJSON()
                        }));
                    }
                })

            }});

        return this;
    }
});