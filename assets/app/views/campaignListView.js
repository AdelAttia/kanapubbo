window.CampaignListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteCampaign'
    },

    render: function () {
        var campaigns = new CampaignCollection();
        var that = this;
        campaigns.fetch({
            success: function() {
                that.$el.html(that.template({
                    campaigns: campaigns.toJSON()
                }));
            }
        });
        return that;
    },

    deleteCampaign: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var campaign = new Campaign( {id : $(ev.currentTarget).data("id")} )
                campaign.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});