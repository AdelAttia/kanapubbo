window.UriEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #uriEditForm': 'saveUri'
    },
    saveUri: function (ev) {
        var uriDetails = $(ev.currentTarget).serializeObject();
        var uri = new Uri();
        uri.save(uriDetails, {
            success: function () {
                Backbone.history.navigate('#uris', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var uri = new Uri({id: id});
            uri.fetch({
                success: function(uri) {

                    var site = new PublisherWebsite();
                    site.fetch({
                        success: function(sites) {

                            that.$el.html(that.template({
                                uri: uri.toJSON(),
                                sites: sites.toJSON()
                            }));


                    }});
            }});
        } else {


            var site = new PublisherWebsite();
            site.fetch({
                success: function (sites) {

                    that.$el.html(that.template({uri: null, sites: sites.toJSON()}));

                }
            });
        }
        return that;
    }
});