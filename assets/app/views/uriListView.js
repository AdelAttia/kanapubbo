window.UriListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteUri'
    },

    render: function () {
        var uris = new UriCollection();
        var that = this;
        uris.fetch({
            success: function() {

                var site = new PublisherWebsite();
                site.fetch({
                    success: function(sites) {



                        that.$el.html(that.template({
                            uris: uris.toJSON(),
                            sites: sites.toJSON()
                        }));

                }});
        }});
        return that;
    },

    deleteUri: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var uri = new Uri( {id : $(ev.currentTarget).data("id")} )
                uri.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});