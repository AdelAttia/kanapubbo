window.UserEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #userEditForm': 'saveUser'
    },
    saveUser: function (ev) {
        var userDetails = $(ev.currentTarget).serializeObject();
        var user = new User();
        user.save(userDetails, {
            success: function () {
                Backbone.history.navigate('#users', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var user = new User({id: id});
            //var user = User.find({id : id});
            user.fetch({
                success: function(user) {
                    that.$el.html(that.template({
                        user: user.toJSON()
                    }));
                }
            });
        } else {
            that.$el.html(that.template({ user: null }));
        }

        return that;
    }
});