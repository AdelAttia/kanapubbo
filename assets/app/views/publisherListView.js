window.PublisherListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deletePublisher'
    },

    render: function () {
        var publishers = new PublisherCollection();
        var that = this;
        publishers.fetch({
            success: function() {
                that.$el.html(that.template({
                    publishers: publishers.toJSON()
                }));
            }
        });
        return that;
    },

    deletePublisher: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var publisher = new Publisher( {id : $(ev.currentTarget).data("id")} )
                publisher.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});