window.UserListView = Backbone.View.extend({

    tagName: 'li',

    className: 'dropdown',


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteUser'
    },

    render: function () {
        var users = new UserCollection();
        var that = this;
        users.fetch({
            success: function() {
                that.$el.html(that.template({
                    users: users.toJSON()
                }));
            }
        });
        return that;
    },

    deleteUser: function (ev) {
        var that = this;


        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var user = new User( {id : $(ev.currentTarget).data("id")} )
                user.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render()
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });


        return false;
    }
});