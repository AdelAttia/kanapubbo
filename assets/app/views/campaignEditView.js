window.CampaignEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #campaignEditForm': 'saveCampaign'
    },
    saveCampaign: function (ev) {
        var campaignDetails = $(ev.currentTarget).serializeObject();
        var campaign = new Campaign();
        campaign.save(campaignDetails, {
            success: function () {
                Backbone.history.navigate('#campaigns', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var campaign = new Campaign({id: id});
            //var campaign = Campaign.find({id : id});
            campaign.fetch({
                success: function(campaign) {
                    console.log(campaign)
                    that.$el.html(that.template({
                        campaign: campaign.toJSON()
                    }));
                    //$('.date').datepicker({});

                }
            });
        } else {
            that.$el.html(that.template({ campaign: null }));
            //$('.date').datepicker({});
        }

        return that;
    }


});