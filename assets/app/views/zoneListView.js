window.ZoneListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteZone'
    },

    render: function () {
        var zone = new Zone();
        var that = this;
        zone.fetch({
            success: function(zones) {

            var dimension = new Dimension();
            dimension.fetch({
                success: function(dimensions) {

                var zoneType = new ZoneType();
                zoneType.fetch({
                    success: function(zonesTypes) {

                    var uri = new Uri();
                    uri.fetch({
                        success: function(uris) {

                            that.$el.html(that.template({
                                zones       : zones .toJSON(),
                                dimensions : dimensions.toJSON(),
                                types      : zonesTypes .toJSON(),
                                uris       : uris .toJSON()
                            }));
                    }});
                }});
            }});
        }});
        return that;
    },

    deleteZone: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var zone = new Zone( {id : $(ev.currentTarget).data("id")} )
                zone.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});