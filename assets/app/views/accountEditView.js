window.AccountEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #accountEditForm': 'saveAccount'
    },
    saveAccount: function (ev) {
        var accountDetails = $(ev.currentTarget).serializeObject();
        var account = new Account();
        account.save(accountDetails, {
            success: function () {
                Backbone.history.navigate('#accounts', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var account = new Account({id: id});
            //var account = Account.find({id : id});
            account.fetch({
                success: function(account) {
                    that.$el.html(that.template({
                        account: account.toJSON()
                    }));
                }
            });
        } else {
            that.$el.html(that.template({ account: null }));
        }

        return that;
    }
});