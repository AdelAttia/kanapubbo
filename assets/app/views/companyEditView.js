window.CompanyEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #companyEditForm': 'saveCompany',
        'change #account': 'changeAccountInfo'
    },
    saveCompany: function (ev) {
        //console.log($(ev.currentTarget).find("#name"));
        var companyDetails = {
            "account": {
                "name": $("#account-name").val(),
                "code": $("#account-code").val(),
                "unit": $("#account-unit").val(),
                "balance": $("#account-balance").val(),
                "polarity": $('input[name=account-polarity]:checked').val(),
                "enforce_positive_balance": $('input[name=account-enforce_positive_balance]:checked').val()
            },
            "adress": {
                "street_line1": $("#adress-street_line1").val(),
                "street_line2": $("#adress-street_line2").val(),
                "zipcode": $("#adress-zipcode").find(":selected").val()
            },
            "fax": $("#fax").val(),
            "name": $("#name").val(),
            "phone": $("#phone").val()
            };

        if($("#id").val()) {
            companyDetails.account.id = $("#account-id").val();
            companyDetails.adress.id  = $("#adress-id").val();
            var company = new Company({id: $("#id").val() });
        } else {
            var company = new Company();
        }
        company.save(companyDetails, {
            success: function () {
                Backbone.history.navigate('#companies', {trigger:true});
            }
        });

        return false;
    },

    changeAccountInfo : function (ev) {
        var selectedValue = $(ev.currentTarget).find(":selected").val();
        var account = new Account({id: selectedValue});
        account.fetch({
            success: function(account) {
                RadionButtonSelectedValueSet("account-polarity", account.get("polarity"));
                RadionButtonSelectedValueSet("account-enforce_positive_balance", account.get("enforce_positive_balance"));
                $("#account-name").val(account.get("name"));
                $("#account-code").val(account.get("code"));
                $("#account-unit").val(account.get("unit"));
                $("#account-balance").val(account.get("balance"));
            }
        });

        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var company = new Company({id: id});
            company.fetch({
                success: function(company) {

                    var zipcode = new Zipcode();
                    zipcode.fetch({
                        success: function(zipcodes) {

                            var account = new Account();
                            account.fetch({
                                success: function(accounts) {

                                    that.$el.html(that.template({
                                        company: company.toJSON(),
                                        zipcodes : zipcodes.toJSON(),
                                        accounts : accounts.toJSON()
                                    }));

                            }});

                    }});

            }});
        } else {
            var zipcode = new Zipcode();
            zipcode.fetch({
                success: function(zipcodes) {

                    var account = new Account();
                    account.fetch({
                        success: function(accounts) {

                            that.$el.html(that.template({
                                company : null,
                                zipcodes : zipcodes.toJSON(),
                                accounts : accounts.toJSON()
                            }));

                        }});

                }});
        }

        return that;
    }
});