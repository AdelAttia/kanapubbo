window.ZoneEditView = Backbone.View.extend({


    initialize : function() {
        //this.render()
    },

    events: {
        'submit #zoneEditForm': 'saveZone'
    },
    saveZone: function (ev) {
        var zoneDetails = $(ev.currentTarget).serializeObject();
        var zone = new Zone();
        zone.save(zoneDetails, {
            success: function () {
                Backbone.history.navigate('#zones', {trigger:true});
            }
        });
        return false;
    },

    render: function (id) {
        var that = this;
        if(id) {
            var zone = new Zone({id: id});
            zone.fetch({
                success: function(zone) {

                var dimension = new Dimension();
                    dimension.fetch({
                    success: function(dimensions) {

                    var zoneType = new ZoneType();
                    zoneType.fetch({
                        success: function(zonesTypes) {

                        var uri = new Uri();
                        uri.fetch({
                            success: function(uris) {

                                that.$el.html(that.template({
                                    zone       : zone .toJSON(),
                                    dimensions : dimensions.toJSON(),
                                    types      : zonesTypes .toJSON(),
                                    uris       : uris .toJSON()
                                }));
                        }});
                    }});
                }});

            }});
        } else {

            var dimension = new Dimension();
            dimension.fetch({
                success: function(dimensions) {

                var zoneType = new ZoneType();
                zoneType.fetch({
                    success: function(zonesTypes) {

                    var uri = new Uri();
                    uri.fetch({
                        success: function(uris) {

                            that.$el.html(that.template({
                                zone       : null,
                                dimensions : dimensions.toJSON(),
                                types      : zonesTypes .toJSON(),
                                uris       : uris .toJSON()
                            }));

                    }});
                }});
            }});
        }
        return that;
    }
});