window.PublisherWebsiteListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deletePublisherWebsite'
    },

    render: function () {
        var publisherWebsites = new PublisherWebsiteCollection();
        var that = this;
        publisherWebsites.fetch({
            success: function() {
                that.$el.html(that.template({
                    websites: publisherWebsites.toJSON()
                }));
            }
        });
        return that;
    },

    deletePublisherWebsite: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var publisherWebsite = new PublisherWebsite( {id : $(ev.currentTarget).data("id")} )
                publisherWebsite.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});