window.BannerListView = Backbone.View.extend({


    initialize : function() {
        this.render()
    },

    events: {
        'click .delete': 'deleteBanner'
    },

    render: function () {
        var that = this;
        var banner = new Banner();
        banner.fetch({
            success: function(banner) {

                var dimension = new Dimension();
                dimension.fetch({
                    success: function(dimensions) {

                        var bannerType = new BannerType();
                        bannerType.fetch({
                            success: function(bannersTypes) {

                                that.$el.html(that.template({
                                    banners     : banner .toJSON(),
                                    dimensions : dimensions.toJSON(),
                                    types      : bannersTypes .toJSON()
                                }));
                            }});
                    }});
            }});

        return that;
    },

    deleteBanner: function (ev) {
        var that = this;
        swal({
            title: "Êtes-vous sûr ?",
            text: "Êtes-vous sûr de vouloir supprimer définitivement cet élément ?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4f8edc",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var banner = new Banner( {id : $(ev.currentTarget).data("id")} )
                banner.destroy({
                    success: function () {
                        swal("Supprimé!", "L'élément a été supprimé avec succès.", "success");
                        that.render();
                    }
                });
            } else {
                swal("Annulé", "L'opération a été annulée.", "error");
            }
        });
        return false;
    }
});