
var token = getData("kanapubToken");

var userInfo = JSON.parse(getData("userInfo"));

$.ajaxSetup({
    headers: {
        "accept": "application/json",
        "Authorization": "JWT " + token
    },
    statusCode: {
        401: function(){
            window.location.href = 'login.html';
        }
    }
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

window.hostServer = "http://vps130625.ovh.net/api/";
window.templateLoader = {

    load: function(views, callback) {

        var deferreds = [];

        $.each(views, function(index, view) {
            if (window[view]) {
                deferreds.push($.get('tpl/' + view + '.html', function(data) {
                    window[view].prototype.template = _.template(data);
                }, 'html'));
            } else {
                aa(view + " not found");
            }
        });

        $.when.apply(null, deferreds).done(callback);
    }

};

function RadionButtonSelectedValueSet(name, SelectdValue) {
    $('input[name="' + name+ '"][value="' + SelectdValue + '"]').prop('checked', true);
}



function saveData(key, data) {
    if (window.localStorage) {
        window.localStorage.setItem(key, data);
    } else {
        $.cookie(key, data);
    }
}

function getData(key) {
    if (window.localStorage) {
        data = window.localStorage.getItem(key);
    } else {
        data = $.cookie(key);
    }
    return data;
}